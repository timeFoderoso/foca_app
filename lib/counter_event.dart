//As interações do usuário com o contador. Ele só pode incrementar ou decrementar.

enum CounterEvent {increment, decrement}