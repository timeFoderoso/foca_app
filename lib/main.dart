import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foca/counter_bloc.dart';
import 'package:foca/counter_event.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Exemplo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      //Widget que fornece o bloc para todos os outros descendentes dele.
      home: BlocProvider(
        builder: (context) => CounterBloc(),
        child: MyHomePage(title: 'Exemplo'),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        //Widget que fornece o estado atual do bloc. Ele vai pegar o bloc CounterBloc fornecido pelo
        //BlocProvider acima.
        child: BlocBuilder<CounterBloc, int>(
          //O builder tem um atributo "state". Nesse exemplo, ele é um inteiro.
          builder: (context, state) => Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Valor do contador: ',
              ),
              Text(
                state.toString(),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          FloatingActionButton(
            child: Icon(Icons.add),
            onPressed: () {
              //Manda o evento de incremento para o bloc.
              BlocProvider.of<CounterBloc>(context)
                  .dispatch(CounterEvent.increment);
            },
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 10),
          ),
          FloatingActionButton(
            child: Icon(Icons.remove),
            onPressed: () {
              //Manda o evento de decremento para o bloc.
              BlocProvider.of<CounterBloc>(context)
                  .dispatch(CounterEvent.decrement);
            },
          ),
        ],
      ),
    );
  }
}