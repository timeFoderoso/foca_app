import 'package:foca/counter_event.dart';
import 'package:bloc/bloc.dart';

//Classe que pega eventos(interações do usuário) e muda o estado.

class CounterBloc extends Bloc<CounterEvent, int> {

  //Como diz o nome, o estado inicial (0)
  @override
  int get initialState => 0;

  //Essa função recebe um evento e muda o estado. É uma função
  //assíncrona. Internamente esse plugin pega oq ta dps do yield e bota
  //como o estado atual. Todo bloc vai seguir exatamente essa estrutura.

  //currentState é uma variável que guarda o estado atual. Inicialmente ele começa
  //como 0 (definido acima). De acordo com o evento, muda o estado.
  @override
  Stream<int> mapEventToState(CounterEvent event) async* {
    if (event == CounterEvent.increment) {
      yield currentState + 1;
    } else if (event == CounterEvent.decrement) {
      yield currentState - 1;
    }
  }
}
